# Batch download app

    * this is a fullstack version
    * downloaded file is stored in `./downloads` in this repository (configure in `docker-compose.yaml`)
  
## Project Overview

![demo](diagram.png)

## Demo

![demo](screen.gif)

## Get started

```bash
docker-compose --compatibility up --build -d 
open localhost:5000
```
