const mongoose = require('mongoose');
require('./models/profile');
const Profile = mongoose.model('Profile');

const profiles = [
    {
        "protocol": "sftp",
        "hostname": "test.rebex.net",
        "username": "demo",
        "password": "password",
        "name": "rebex"
    },
    {
        "protocol": "ftp",
        "hostname": "test.rebex.net",
        "username": "demo",
        "password": "password",
        "name": "rebex"
    }
]

module.exports = {
    seed: () => {
        Profile.find()
            .then(r => {
                if (!r.length) Profile.insertMany(profiles)
            })
    }
}