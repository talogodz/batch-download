require('dotenv').config();
const mongoose = require('mongoose');
const amqp = require('amqplib');
const app = require('express')();
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan')
const seeder = require('./seed');
const e = require('express');
const http = require('http').createServer(app);
const io = require('socket.io')(http, { cors: { origin: "*" } });
const port = process.env.PORT || 5050;

require("fs").readdirSync('./models').forEach(file => {
    require("./models/" + file);
});

app.use(morgan('tiny'))
app.use(cors());
app.use(bodyParser.json());

app.use('/api', require('./api'));
app.set('io', io);

app.use(function (err, req, res, next) {
    console.error(err.stack)
    res.status(500).json({ message: err.message });
})

const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    user: process.env.DB_USER,
    pass: process.env.DB_PASS,
    auth: { authSource: "admin" }
};

amqp.connect(process.env.MQ_URL).then(connection => { //queue
    connection.createConfirmChannel().then((channel) => {
        app.set('queueConnection', connection);
        app.set('queueChannel', channel);
        mongoose.connect(process.env.MONGO_URL, options, (err) => { //mongo
            if (err) throw err;
            seeder.seed();
            http.listen(port, () => {
                console.log(`api is listening on http://localhost:${port}`);
            });
        })
    });
});


process.on('exit', () => {
    app.get('queueChannel').close();
    app.get('queueConnection').close();
})
