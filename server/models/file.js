const mongoose = require('mongoose');

const FileSchema = new mongoose.Schema({
    ref: { type: String, required: true },
    name: { type: String, required: true },
    savedName: { type: String },
    href: { type: String, required: true },
    status: { type: String }
}, { timestamps: true });

FileSchema.pre('insertMany', (next, docs) => {
    docs.map(preSave);
    return next();
});

const preSave = (doc) => {
    if (!doc.savedName) doc.savedName = getSavedName(doc.name, doc.ref);
    if (!doc.status) doc.status = 'pending';
    return doc;
}

const getSavedName = (name, ref) => {
    const [fileName, ext] = name.split('.');
    let savedName = `${fileName}-${ref}`;
    if (ext) savedName = `${savedName}.${ext}`;
    return savedName;
}

mongoose.model('File', FileSchema);