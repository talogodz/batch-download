const mongoose = require('mongoose');

const ProfileSchema = new mongoose.Schema({
    protocol: { type: String, required: true },
    hostname: { type: String, required: true },
    username: { type: String, required: true },
    password: { type: String, required: true },
    name: { type: String, required: true }
}, { timestamps: true });

mongoose.model('Profile', ProfileSchema);