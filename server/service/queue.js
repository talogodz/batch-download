const amqp = require('amqplib');

module.exports = {
    send: async (channel, queue, messages) => {
        channel.assertQueue(queue, { durable: true });
        messages.forEach(msg => {
            channel.sendToQueue(queue, Buffer.from(JSON.stringify(msg)), {
                persistent: true
            }, (err, _) => {
                if (!err) {
                    console.log(` [x] Q sent ${JSON.stringify(msg)}`);
                } else console.log(err);
            });
        });
    }
};
