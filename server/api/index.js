const router = require('express').Router();

router.use('/files', require('./files'));
router.use('/profiles', require('./profiles'));

module.exports = router;
