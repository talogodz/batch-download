const router = require('express').Router();
const mongoose = require('mongoose');
const Profile = mongoose.model('Profile');

router.get('/', (req, res, next) => {
    Profile.find()
        .then((r) => res.json(r))
        .catch(next);
});

router.post('/', (req, res, next) => {
    new Profile(req.body).save()
        .then(r => res.json(r))
        .catch(next);
});


module.exports = router;