const router = require('express').Router();
const queueService = require('../../service/queue');
const mongoose = require('mongoose');
const File = mongoose.model('File');

router.get('/', (req, res, next) => {
    File.find()
        .then((r) => res.json(r))
        .catch(next);
});

router.post('/', async (req, res, next) => {
    const savedFiles = await File.insertMany(req.body);
    const queueFiles = req.body.map(queueFile => {
        const [savedFile] = savedFiles.filter(f => queueFile.ref == f.ref);
        queueFile.savedName = savedFile.savedName;
        return queueFile;
    })
    console.log(queueFiles);
    queueService.send(req.app.get('queueChannel'), 'task', queueFiles);

    return res.json(savedFiles);

});

router.post('/:ref/status', async (req, res, next) => {
    const io = req.app.get('io');
    const { status } = req.body;
    if (!status) throw (new Error('status required'));
    const { ref } = req.params;
    const file = await File.findOne({ ref }).exec();
    let newFile = new File(file);
    newFile._id = mongoose.Types.ObjectId();
    newFile.status = status;
    File.insertMany([newFile]).then(r => res.json(r[0])).catch(next);
    io.emit('message', { ref, status });

});

module.exports = router;