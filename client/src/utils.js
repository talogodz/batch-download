const { parse } = require('path');

const splitUrls = (urls) => {
    if (typeof (urls) != "string") return null;
    return urls.split(/,| /).filter(e => e != "");
}

const getFilename = (urlStr) => {
    if (typeof (urlStr) != "string") return null;
    return urlStr.substring(urlStr.lastIndexOf('/') + 1, urlStr.length);
}

const getHostname = (urlStr) => {
    return urlStr.split('/')[2] || null;
}

const isUrlValid = (urlStr) => {
    try {
        const url = new URL(urlStr);
        return (url.hostname && url.hostname != "") || getHostname(urlStr)
    } catch (_) {
        return false
    }
}

const getUrlComponent = (urlStr) => {
    if (typeof (urlStr) != "string") return null;

    const url = new URL(urlStr);
    const hostname = (url.hostname && url.hostname != "") ? url.hostname : getHostname(urlStr);

    let protocol = url.protocol.replace(":", "");
    if (protocol == "https") protocol = "http";

    let path = url.pathname;
    if (protocol == "sftp") {
        path = urlStr.substring(urlStr.lastIndexOf(hostname) + hostname.length, urlStr.length)
    }

    return {
        url: url.href,
        protocol: protocol,
        hostname,
        filename: getFilename(url.pathname),
        path,
        port: url.port,
    }
}

const utils = {
    splitUrls,
    getFilename,
    getUrlComponent,
    isUrlValid,
    getHostname
}

module.exports = utils;