const Client = require('ssh2-sftp-client');
const fs = require('fs');

module.exports = {
    download: (fileObject, dest, onUpdate, onError) => {
        console.log('sftp protocol:')

        if (!fileObject.profile) {
            onError(fileObject.ref);
            return;
        }

        const sftp = new Client();
        sftp.connect({
            host: fileObject.hostname,
            username: fileObject.profile.username,
            password: fileObject.profile.password
        }).then(() => {
            onUpdate('downloading', fileObject.ref)
            sftp.get(fileObject.path, fs.createWriteStream(`${dest}/${fileObject.savedName}`))
                .then(() => {
                    onUpdate('finish', fileObject.ref)
                    sftp.end();
                }).catch(() => onError(fileObject.ref));
        }).catch(() => onError(fileObject.ref));

    }
}