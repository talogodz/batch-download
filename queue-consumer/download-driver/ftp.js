const Client = require('ftp');
const fs = require('fs');

module.exports = {
    download: (fileObject, dest, onUpdate, onError) => {
        console.log('ftp protocol:')

        const client = new Client();
        client.connect({
            host: fileObject.hostname,
            user: fileObject.profile.username || 'anonymous',
            password: fileObject.profile.password || 'anonymous@'
        });
        client.on('ready', () => {
            onUpdate('downloading', fileObject.ref)
            client.get(fileObject.path, (err, stream) => {
                if (err) onError(fileObject.ref);
                else {
                    stream.once('close', () => { client.end(); });
                    stream.pipe(fs.createWriteStream(`${dest}/${fileObject.savedName}`))
                        .on('finish', () => onUpdate('finish', fileObject.ref))
                        .on('error', () => onError(fileObject.ref));
                }
            });
        });
        client.on('error', () => onError(fileObject.ref));
    }
}