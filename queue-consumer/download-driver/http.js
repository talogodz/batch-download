const fs = require('fs');
const request = require('request');

module.exports = {
    download: (fileObject, dest, onUpdate, onError) => {
        console.log('http protocol:')
        onUpdate('downloading', fileObject.ref);
        request(fileObject.href)
            .on('response', (response) => {
                response.pipe(fs.createWriteStream(`${dest}/${fileObject.savedName}`))
                    .on('finish', () => onUpdate('finish', fileObject.ref))
                    .on('error', (err) => onError(fileObject.ref));
            })
            .on('error', (err) => onError(fileObject.ref));
    }
}