require('dotenv').config();
const amqp = require('amqplib');
const fetch = require("node-fetch");
const fs = require('fs');
const queue = 'task';
const drivers = new Map();
fs.readdirSync('./download-driver').forEach(file => {
    const [name] = file.split('.');
    drivers[name] = require(`./download-driver/${file}`);
});
const dest = './downloads';

amqp.connect(process.env.MQ_URL).then(connection => {
    connection.createChannel().then(channel => {
        console.log(" [x] Listening for message");

        channel.assertQueue(queue, { durable: true });
        channel.prefetch(1);

        channel.consume(queue, (msg) => {
            const fileObject = JSON.parse(msg.content.toString());
            try {
                console.log(fileObject);
                drivers[fileObject.protocol].download(
                    fileObject,
                    dest,
                    (status, ref) => {
                        onUpdate(ref, status, process.env.API_URL);
                        if (status == 'finish') channel.ack(msg);
                    },
                    (ref) => {
                        onUpdate(ref, 'fail', process.env.API_URL);
                        channel.ack(msg);
                    })
            } catch (err) {
                onUpdate(fileObject.ref, 'fail', process.env.API_URL);
                channel.ack(msg);
            }
        }, {
            noAck: false
        });
    });
});

const onUpdate = (ref, status, apiUrl) => {
    console.log(status)
    fetch(`${apiUrl}/api/files/${ref}/status`, {
        method: "POST",
        body: JSON.stringify({ status }),
        headers: { "Content-Type": "application/json" }
    });
}